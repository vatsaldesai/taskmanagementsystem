﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TaskModel.Admin
{
    public class User
    {

        public int EmpId { get; set; }
        public string EmpName { get; set; }
        public int DesignationId { get; set; }
        public Nullable<System.DateTime> BirthDate { get; set; }
        public Nullable<System.DateTime> JoiningDate { get; set; }
        public Nullable<decimal> Salary { get; set; }
        public string Email { get; set; }
        public string Password { get; set; }
        public Nullable<int> DeptId { get; set; }
        public Nullable<int> RoleId { get; set; }
        public Nullable<bool> IsActive { get; set; }
        public  Department Department { get; set; }
      
        public  UserRole UserRole { get; set; }
        public  EmpDesignation EmpDesignation { get; set; }
        public virtual ICollection<UserProject> UserProjects { get; set; }
        public virtual ICollection<UserTask> UserTasks { get; set; }
        public string RequestUrl { get; set; }
        public string ConfirmPassword { get; set; }
    }
    public class EmpDesignation
    {
        public int DesignationId { get; set; }
        public string Designation { get; set; }
        public virtual ICollection<User> Users { get; set; }
    }
    public partial class UserTask
    {
        public int UserTaskId { get; set; }
        public Nullable<int> TaskId { get; set; }
        public Nullable<int> UserId { get; set; }

        public virtual Task Task { get; set; }
        public virtual User User { get; set; }


    }
    public class Department
    {
        public int DeptId { get; set; }
        public string DeptName { get; set; }

      
    }
    public class Project
    {
        public int ProjectId { get; set; }
        public string ProjectName { get; set; }
        public string ShortName { get; set; }
        public string Description { get; set; }
        public string EstimateHours { get; set; }
        public Nullable<System.DateTime> StartDate { get; set; }
        public Nullable<System.DateTime> EndDate { get; set; }
        public Nullable<int> CreatedBy { get; set; }
        public Nullable<int> TaskTypeId { get; set; }
        public Nullable<int> StatusId { get; set; }
        public Nullable<System.DateTime> CreatedDate { get; set; }
        public Nullable<System.DateTime> ModifiedDate { get; set; }
        public virtual ICollection<UserProject> UserProjects { get; set; }
        public virtual User User { get; set; }
        public virtual Status Status { get; set; }
        public virtual ICollection<Task> Tasks { get; set; }
    }
    public  class TaskType
    {
        public int TaskTypeId { get; set; }
        public string TaskTypeName { get; set; }
    }
    public class UserRole
    {
        public int RoleId { get; set; }
        public string UserRoleName { get; set; }
        public virtual ICollection<User> Users { get; set; }
    }
    public class GeneralDto
    {
    
        public User User { get; set; }
        public List<Department> Departments { get; set; }
        public List<UserRole> UserRole { get; set; }
        public List<EmpDesignation> EmpDesignation { get; set; }
    }
    public class ProjectDTO
    {
        public Project Project { get; set; }
        public List<UserProject> UsersOfProject { get; set; }
        public List<User> Users { get; set; }
        public List<TaskType> TaskTypes { get; set; }
        public List<Status> Status { get; set; }
    }
    public partial class Status
    {
        public int StatusId { get; set; }
        public string StatusName { get; set; }
    }
    public class TaskDTO
    {
        public Task Task { get; set; }
        public List<UserTask> UsersOfTask { get; set; }

        public List<Project> Projects { get; set; }
        public List<User> Users { get; set; }
    }
    public partial class Task
    {
        public int TaskId { get; set; }
        public string TaskName { get; set; }
        public Nullable<System.DateTime> TaskDate { get; set; }
        public Nullable<System.DateTime> EstimateDate { get; set; }
        public string TaskDetails { get; set; }
        public Nullable<int> ProjectId { get; set; }
        public Nullable<int> CreatedBy { get; set; }
        public string Status { get; set; }
        public Nullable<int> ModifiedBy { get; set; }
        public virtual Project Project { get; set; }
        public virtual ICollection<UserTask> UserTasks { get; set; }
        public virtual User User { get; set; }
        public virtual User User1 { get; set; }
    }

    public partial class UserProject
    {
        public int ProjectUserId { get; set; }
        public int ProjectId { get; set; }
        public int UserId { get; set; }

        public virtual User User { get; set; }
    }
    public class EmailDto
    {
        public string To { get; set; }
        public string From { get; set; }
        public string Subject { get; set; }
        public string Body { get; set; }
        public string Host { get; set; }
        public string Port { get; set; }
        public string Username { get; set; }
        public string Password { get; set; }
        public bool SmtpSSl { get; set; }
        public string ApplicationURL { get; set; }
        public string ApplicationPhysicalPath { get; set; }
    }
    public enum AlphabetNumericCheck
    {
        AllLetters = 1,
        AlphaNumeric,
        OnlyNumeric
    }
}