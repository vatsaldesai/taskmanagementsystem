﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TaskModel
{
    public class SessionUtility
    {
        public static int UserId
        {
            get
            {
                if (HttpContext.Current.Session["UserId"] == null)
                    return 0;
                return int.Parse(HttpContext.Current.Session["UserId"].ToString());
            }
            set
            {
                HttpContext.Current.Session["UserId"] = value;
            }
        }
        public static string UserName
        {
            get
            {
                if (HttpContext.Current.Session["UserName"] == null)
                    return "";
                return HttpContext.Current.Session["UserName"].ToString();
            }
            set
            {
                HttpContext.Current.Session["UserName"] = value;
            }
        }
        public static int? UserRoleId
        {
            get
            {
                if (HttpContext.Current.Session["UserRoleId"] == null)
                    return 0;
                return Convert.ToInt32(HttpContext.Current.Session["UserRoleId"]);
            }
            set
            {
                HttpContext.Current.Session["UserRoleId"] = value;
            }
        }
    }
}