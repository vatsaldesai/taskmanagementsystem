﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Formatting;
using System.Web;

namespace TaskModel
{
    public class WebApiUtility
    {
        public static IEnumerable<T> GetWebApiCall<T>(string GetURL)
        {
            IEnumerable<T> resultResponse = null;
            try
            {
               
                using (var client = new HttpClient())
                {
                    using (HttpResponseMessage response = client.GetAsync(GetURL).Result)
                    {
                        if (response.IsSuccessStatusCode)
                        {
                            resultResponse = response.Content.ReadAsAsync<IEnumerable<T>>().Result;
                        }
                    }
                }
                return resultResponse;
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public static IEnumerable<T> PostWebApiCall<T>(T ModelObject, string PostURL)
        {
            dynamic resultResponse = null;
            try
            {
              
                using (var client = new HttpClient())
                {
                    using (HttpResponseMessage response = client.PostAsync(PostURL, ModelObject, new JsonMediaTypeFormatter()).Result)
                    {
                        if (response.IsSuccessStatusCode)
                        {
                            resultResponse = response.Content.ReadAsAsync<IEnumerable<T>>().Result;
                        }
                    }
                }
            }
            catch (Exception e)
            {
                throw e;
            }
            return resultResponse;
        }

        public static T2 PostWebApiCall<T, T2>(T ModelObject, string PostURL)
        {
            dynamic resultResponse = null;
            try
            {
           
                using (var client = new HttpClient())
                {
                    using (HttpResponseMessage response = client.PostAsync(PostURL, ModelObject, new JsonMediaTypeFormatter()).Result)
                    {
                        if (response.IsSuccessStatusCode)
                        {
                            resultResponse = response.Content.ReadAsAsync<T2>().Result;
                        }
                    }
                }
                return resultResponse;
            }
            catch (Exception e)
            {
                throw e;
            }
        }
    }
}