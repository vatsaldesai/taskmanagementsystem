﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mail;
using System.Web;
using TaskModel.Admin;

namespace TaskModel
{
    public class CommonMethods
    {
        public static void SendEmail(User objLogin, EmailDto objConfiguration, string HtmlBody, string PracticeAdminEmail, string EmailTo, string DisplaName, string Subject)
        {
            MailMessage mail = new MailMessage();
          
            try
            {
                mail.To.Add(EmailTo);
                mail.From = new MailAddress(PracticeAdminEmail);
                mail.Subject = Subject;
                mail.Body = HtmlBody;
                mail.IsBodyHtml = true;
                SmtpClient smtp = new SmtpClient();
                smtp.Host = objConfiguration.Host;
                smtp.Port = Convert.ToInt32(objConfiguration.Port);
                smtp.Credentials = new System.Net.NetworkCredential
                (objConfiguration.Username, objConfiguration.Password);
                smtp.UseDefaultCredentials = true;
                smtp.EnableSsl = Convert.ToBoolean(objConfiguration.SmtpSSl);
                smtp.Send(mail);
            }
            catch (Exception e)
            {
                throw e;
            }
        }
    }
}