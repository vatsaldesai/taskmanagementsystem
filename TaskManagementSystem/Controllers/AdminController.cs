﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using TaskModel;
using TaskModel.Admin;

namespace TaskManagementSystem.Controllers
{
    public class AdminController : Controller
    {
        HttpClient client = new HttpClient();
       // EncryptDecryptKey objEncryptDecrypt = new EncryptDecryptKey();
        // GET: Admin
        public ActionResult Index()
        {
            return View();
        }

        [AllowAnonymous]
        public ActionResult Login()
        {
            return View();
        }
        [HttpPost]
        [AllowAnonymous]
        public ActionResult Login(User model)
        {

            if (ModelState.IsValid)
            {
               // client.BaseAddress = new Uri("http://localhost:30013");
                //StringContent content = new StringContent(JsonConvert.SerializeObject(model), System.Text.Encoding.UTF8, "application/json");
                IEnumerable<User> objLogin = WebApiUtility.PostWebApiCall<User,List<User>>(model, "http://localhost:30013/api/Admin/GetLoginDetails");
               

                if (objLogin != null && objLogin.ToList().Count > 0)
                {
                    SessionUtility.UserName = objLogin.FirstOrDefault().EmpName;
                    SessionUtility.UserId = objLogin.FirstOrDefault().EmpId;
                    SessionUtility.UserRoleId = objLogin.FirstOrDefault().RoleId;                                                                                              
                    if (objLogin.ToList().FirstOrDefault().IsActive == false)
                    {
                        ModelState.AddModelError("InvalidInfo", "You can not login now, Please contact your Security Administrator");
                        return View(model);
                    }

                }
                else
                {
                    ModelState.AddModelError("InvalidInfo", "Account doesn't exist.");
                    return View(model);
                }
                
            }
            
            return View("../Admin/Dashboard", model);
        }

        public ActionResult Dashboard(User model)
        {
            WebApiUtility.PostWebApiCall<Project, List<ProjectDTO>>(null, "http://localhost:30013/api/Admin/GetAllProjects");
            return View();

        }

        public ActionResult Projects()
        {
            IEnumerable<ProjectDTO> objProject = WebApiUtility.PostWebApiCall<Project,List<ProjectDTO>>(null, "http://localhost:30013/api/Admin/GetAllProjects");
            //client.BaseAddress = new Uri("http://localhost:30013");
            // StringContent content = new StringContent(JsonConvert.SerializeObject(model), System.Text.Encoding.UTF8, "application/json");
            //HttpResponseMessage res = client.PostAsync("/api/Admin/GetAllProjects", null).Result;
            //if (res.IsSuccessStatusCode)
            //{
            //    var response = res.Content.ReadAsStringAsync().Result;
            //    project = JsonConvert.DeserializeObject<List<Project>>(response);
            //}
            return View(objProject);
        }
        public ActionResult Task()
        {
            IEnumerable<Task> tasks = WebApiUtility.PostWebApiCall<Task, List<Task>>(null, "http://localhost:30013/api/Admin/GetAllTasks");
            return View(tasks);
        }

        public ActionResult User()
        {
            IEnumerable<User> users = WebApiUtility.PostWebApiCall<User>(null, "http://localhost:30013/api/Admin/GetAllUsers");
            return View(users);
        }

        public PartialViewResult NewProject(Project objProj)
        {
            IEnumerable<ProjectDTO> projectDetails = WebApiUtility.PostWebApiCall<Project, List<ProjectDTO>>(objProj, "http://localhost:30013/api/Admin/GetProjectDetails");
            return PartialView("_NewProject", projectDetails);
        }
        public JsonResult SearchUser(string Prefix)
        {
            User objUser = new User() { EmpName = Prefix };
            IEnumerable<User> users = WebApiUtility.PostWebApiCall<User>(objUser, "http://localhost:30013/api/Admin/SearchUsers");
            return Json(users,JsonRequestBehavior.AllowGet);
        }
        public PartialViewResult NewUser(User objUser)
        {
            User user = new User();
            IEnumerable<GeneralDto> users = WebApiUtility.PostWebApiCall<User,List<GeneralDto>>(objUser, "http://localhost:30013/api/Admin/GetUserDetails");
            return PartialView("_User", users);
        }
        public PartialViewResult NewTask(Task objTask)
        {
            IEnumerable<TaskDTO> tasks = WebApiUtility.PostWebApiCall<Task, List<TaskDTO>>(objTask, "http://localhost:30013/api/Admin/GetTaskdetails");
            return PartialView("_Task", tasks);
        }
        [HttpPost]
        public void InsertUpdateUser(User objUser)
        {
             WebApiUtility.PostWebApiCall<User>(objUser, "http://localhost:30013/api/Admin/InsertUpdateUser");
            if(objUser.EmpId==0)
            {
                string forgotPasswordEmailContent = GetResetPasswordBodyContent(objUser, fillConfigData(), "?p=UserId=" + objUser.EmpId + "&Date=" + DateTime.Now.ToString() + "&Request=Generate", "GeneratePassword.html");
                CommonMethods.SendEmail(objUser, fillConfigData(), forgotPasswordEmailContent, Convert.ToString(Convert.ToString(ConfigurationManager.AppSettings["MailFrom"])), objUser.Email, "Civica (Do Not Reply)", "Generate password for Task Management System");
            }
          
        }
        private EmailDto fillConfigData()
        {
            EmailDto objConfig = new EmailDto();
            objConfig.Host = Convert.ToString(ConfigurationManager.AppSettings["Host"]);
            objConfig.Port = Convert.ToString(ConfigurationManager.AppSettings["Port"]);
            objConfig.Username = Convert.ToString(ConfigurationManager.AppSettings["Username"]);
            objConfig.Password = Convert.ToString(ConfigurationManager.AppSettings["UserPassword"]);
            objConfig.SmtpSSl = Convert.ToBoolean(ConfigurationManager.AppSettings["SmtpSSL"]);
            objConfig.ApplicationURL = Convert.ToString("http://localhost:20285");
            objConfig.ApplicationPhysicalPath = Convert.ToString(ConfigurationManager.AppSettings["ApplicationPhysicalPath"]);
            return objConfig;
        }
        [HttpPost]
        public void InsertUpdateProject(ProjectDTO objProject)
        {

            WebApiUtility.PostWebApiCall<ProjectDTO>(objProject, "http://localhost:30013/api/Admin/InsertUpdateProject");
        }
        [HttpPost]
        public void InsertUpdateTask(TaskDTO objTask)
        {
            WebApiUtility.PostWebApiCall<TaskDTO>(objTask, "http://localhost:30013/api/Admin/InsertUpdateTask");
        }
        [HttpPost]
        public JsonResult GetUsersOfProject(Project objProject)
        {
            IEnumerable<User> usersDetails= WebApiUtility.PostWebApiCall<Project, List<User>>(objProject, "http://localhost:30013/api/Admin/GetUserOfProject");
            return Json(usersDetails,JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public JsonResult GetProjectsByStatus(Project objProject)
        {
            IEnumerable<ProjectDTO> objProjectsdetails = WebApiUtility.PostWebApiCall<Project, List<ProjectDTO>>(objProject, "http://localhost:30013/api/Admin/GetAllProjects");
            return Json(objProjectsdetails, JsonRequestBehavior.AllowGet);
        }
        public string GetResetPasswordBodyContent(User objLogin, EmailDto objConfiguration, string UrlParameters, string Filename)
        {
            string ApplicationURL = Convert.ToString(objConfiguration.ApplicationURL);
            string EmailBodyFilePath = string.Empty;

            EmailBodyFilePath = Path.Combine(Convert.ToString(objConfiguration.ApplicationPhysicalPath), "Areas\\InSyncAdministration\\Views\\Shared\\" + Filename);


            //string UrlParameters = string.Empty;

            //UrlParameters = "?p=" + Server.UrlEncode(objEncryptDecrypt.Encrypt(("UserId=" + objLogin[0].UserId)));
            try
            {
                string FileContents = string.Empty;
                if (System.IO.File.Exists(EmailBodyFilePath))
                {
                    FileContents = System.IO.File.ReadAllText(EmailBodyFilePath);

                    FileContents = FileContents.Replace("#LOGO#", ApplicationURL + "/Images/AdminImages/logoweb.png");
                    FileContents = FileContents.Replace("#NAME#", objLogin.EmpName);
                    FileContents = FileContents.Replace("#RESETURL#", Path.Combine(ApplicationURL, "Admin/GeneratePassword", UrlParameters));
                    FileContents = FileContents.Replace("#LINK#", Path.Combine(ApplicationURL, "Admin/GeneratePassword", UrlParameters));
                    // FileContents = FileContents.Replace("#AdminUser#",userRole);

                }
                return FileContents;
            }
            catch (Exception)
            {
                throw;
            }
        }

        [HttpPost]
        public void AddDepartment(Department objDept)
        {
             WebApiUtility.PostWebApiCall<Department>(objDept, "http://localhost:30013/api/Admin/AddDepartment");
        }
        [HttpPost]
        public void AddDesignation(EmpDesignation objDesg)
        {
            WebApiUtility.PostWebApiCall<EmpDesignation>(objDesg, "http://localhost:30013/api/Admin/AddDesignation");
        }
        [HttpPost]
        public void AddRole(UserRole objRole)
        {
            WebApiUtility.PostWebApiCall<UserRole>(objRole, "http://localhost:30013/api/Admin/AddRole");
        }

        [AllowAnonymous]
        public ActionResult GeneratePassword(string p)
        {
            User objUser = new User();
            if (p != "Forgot")
            {
                // var UserData = objEncryptDecrypt.Decrypt(p).Split('&');
                var UserData = p.Split('&');
                objUser.EmpId = Convert.ToInt32(UserData[0].Replace("UserId=", ""));
                var CreatedOn = Convert.ToDateTime(UserData[1].Replace("Date=", ""));
                objUser.RequestUrl = UserData[2].Replace("Request=", "");
                IEnumerable<GeneralDto> users = WebApiUtility.PostWebApiCall<User, List<GeneralDto>>(objUser, "http://localhost:30013/api/Admin/GetUserDetails");
            }


            return View(objUser);
        }


        [HttpPost]
        [AllowAnonymous]
        //[ValidateAntiForgeryToken]
        public ActionResult GeneratePassword(User model)
        {
            List<User> userdetails = new List<User>();


            if (model.RequestUrl == "Generate" || model.RequestUrl == "ForgotPassword")
            {
                string ErrorMessage = string.Empty;
                User userData = new User();
                userData.EmpId = model.EmpId;
               
               
                             ErrorMessage = ValidateResetPassword(model, 15, 7, AlphabetNumericCheck.AlphaNumeric);
                if (ErrorMessage != "")
                {
                    userdetails.Add(model);
                    ModelState.AddModelError("InvalidInfo", ErrorMessage);
                }
                else
                {
                  WebApiUtility.PostWebApiCall<User>(model, "http://localhost:30013/api/Admin/UpdateUserPassword");
                    ModelState.AddModelError("InvalidInfoSucess", "Password updated successfully.");
                }

            }

           
             



            return View(userdetails.FirstOrDefault());
        }

        private string ValidateResetPassword(User model, int passwordMaxValue, int passwordMinValue, AlphabetNumericCheck alphabetNumericCheckVar)
        {
            string errorMessage = string.Empty;
            //return ErrorMessage;
            if (string.IsNullOrEmpty(model.Password))
            {
                //  return ErrorMessage = "Please enter Password.";
                return "Please enter Password.";
            }

            if (string.IsNullOrEmpty(model.ConfirmPassword))
            {
                //return ErrorMessage = "Please enter Confirm Password.";
                return "Please confirm Password.";
            }

            if (model.Password != model.ConfirmPassword)
            {
                return "Passwords do not match. Please re-enter passwords.";
            }

            if (model.Password.Length < passwordMinValue || model.Password.Length > passwordMaxValue)
            {
                return errorMessage = "Password should contain minimum " + passwordMinValue + " and maximum " + passwordMaxValue + " characters.";
            }

            if (model.Password.ToLower().Contains(model.EmpName))
            {
                return "Password should not contain any part of your first name, last name, or email.";
            }

            switch (alphabetNumericCheckVar)
            {
                case AlphabetNumericCheck.AllLetters:
                    if (!(CheckOnlyAlphabetValue(model.Password)))
                    {
                        return errorMessage = "Password should consist of only alphabets.";
                    }
                    break;
                case AlphabetNumericCheck.AlphaNumeric:
                    if (!(CheckAlphabetNumericValue(model.Password)))
                    {
                        return errorMessage = "Password should consist of only alphanumeric values.";
                    }
                    break;
                case AlphabetNumericCheck.OnlyNumeric:
                    if (!(CheckOnlyNumericValue(model.Password)))
                    {
                        return errorMessage = "Password should consist of only numeric values.";
                    }
                    break;
            }

            return errorMessage;
        }



        private bool CheckAlphabetNumericValue(string newPasswordValue)
        {
            string numStr = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz";
            string charVal = string.Empty;
            string numericVal = newPasswordValue;
            bool stat = false;
            for (int i = 0; i < numericVal.Length; i++)
            {
                charVal = numericVal[i].ToString();
                if (numStr.IndexOf(charVal, 0) != -1)
                {
                    stat = true;
                }
            }

            string numStr1 = "0123456789";
            string char1Val = String.Empty;
            string numericVal1 = newPasswordValue;
            bool stat1 = false;
            for (int i = 0; i < numericVal1.Length; i++)
            {
                char1Val = numericVal1[i].ToString();
                if (numStr1.IndexOf(char1Val, 0) != -1)
                {
                    stat1 = true;
                }
            }
            if (stat && stat1) return true;
            else return false;
        }
        private bool CheckOnlyAlphabetValue(string newPasswordValue)
        {
            string numStr = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz";
            string charVal = string.Empty;
            string numericVal = newPasswordValue;
            bool stat = true;
            for (int i = 0; i < numericVal.Length; i++)
            {
                charVal = numericVal[i].ToString();
                if (numStr.IndexOf(charVal, 0) != -1)
                {
                    stat = true;
                }
                else
                {
                    stat = false;
                    break;
                }
            }
            return stat;
        }
        private bool CheckOnlyNumericValue(string newPasswordValue)
        {
            string numStr = "0123456789";
            string Char = string.Empty;
            string numericval = newPasswordValue;
            bool stat = true;
            for (int i = 0; i < numericval.Length; i++)
            {
                Char = numericval[i].ToString();
                if (numStr.IndexOf(Char, 0) != -1)
                {
                    stat = true;
                }
                else
                {
                    stat = false;
                    break;
                }
            }


            return stat;
        }
    }
}