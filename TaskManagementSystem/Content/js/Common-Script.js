﻿function FormatedDate(jsondate) {
    
    var num = jsondate.match(/\d+/g);

    var date = new Date(parseFloat(num));
    var newdate = date.getMonth() + 1 + "-" + date.getDate() + '-' + date.getFullYear();
    return newdate;
}