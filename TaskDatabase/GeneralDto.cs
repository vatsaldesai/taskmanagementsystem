﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TaskDatabase
{
    public class GeneralDto
    {
        public GeneralDto()
        {
            this.User = new User();
            this.Departments = new List<Department>();
            this.UserRole = new List<UserRole>();
        }
        public User User { get; set; }
        public List<Department> Departments { get; set; }
        public List<UserRole> UserRole { get; set; }
        public List<EmpDesignation> EmpDesignation { get; set; }
    }
    public class ProjectDTO
    {
        public Project Project { get; set; }
        public List<UserProject> UsersOfProject { get; set; }
        public List<User> Users { get; set; }
        public List<TaskType> TaskTypes { get; set; }
        public List<Status> Status { get; set; }
    }

    public class TaskDTO
    {
        public Task Task { get; set; }
        public List<UserTask> UsersOfTask { get; set; }
        public List<Project> Projects { get; set; }
        public List<User> Users { get; set; }
    }
}