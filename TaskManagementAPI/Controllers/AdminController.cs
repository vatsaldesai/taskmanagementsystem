﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using TaskDatabase;
using TaskManagementAPI.Models;
using AutoMapper;
using System.Data.SqlClient;
using System.Data.Entity.Infrastructure;
using System.Text;

namespace TaskManagementAPI.Controllers
{
    public class AdminController : ApiController
    {
        TaskManagementDBEntities ctx;
        public AdminController()
        {
            ctx = new TaskManagementDBEntities();
            ctx.Configuration.ProxyCreationEnabled = false;
        }
        [HttpPost]
         public List<User> GetLoginDetails(User objUser)
        {
            List<User> user = new List<User>();
          
              

                var userdetails = (from c in ctx.Users.Include("Department").Include("EmpDesignation").Include("UserRole")
                                   where c.Email == objUser.Email && c.Password == objUser.Password
                                   select c).AsEnumerable().Select(s => s).ToList();
              //  user.FirstOrDefault().Department = ctx.Departments.Where(s => user.Select(c => c.DeptId).Contains(s.DeptId)).AsEnumerable().Select(s => s).FirstOrDefault();
              //  user.FirstOrDefault().UserTasks = ctx.UserTasks.Where(s => s.UserId == user.FirstOrDefault().EmpId).ToList();
                user = userdetails.ToList();
                //user.FirstOrDefault().Department = new Department();
                //user.FirstOrDefault().Department=userdetails.FirstOrDefault().Department;
                //user.FirstOrDefault().UserRole = new UserRole();
                //user.FirstOrDefault().UserRole = userdetails.FirstOrDefault().UserRole;
          

             return user;
        }

        [HttpPost]
        public List<ProjectDTO> GetAllProjects(Project objProject)
        {
            List<ProjectDTO> projects = new List<ProjectDTO>();
            if(objProject==null || (objProject!=null && objProject.StatusId==null))
            {
               
                var projectDetails = (from c in ctx.Projects.Include("UserProjects").Include("Users")
                                     
                                      select new ProjectDTO
                                      {

                                          Project = c,
                                          UsersOfProject = c.UserProjects.ToList(),
                                          Users = ctx.Users.Where(t => c.UserProjects.Any(c2 => c2.UserId == t.EmpId)).ToList(),
                                          Status = ctx.Status.Select(s => s).ToList(),
                                          TaskTypes = ctx.TaskTypes.Select(s => s).ToList()

                                      }
                                  ).AsEnumerable().Select(s => s).ToList();
                projects = projectDetails;
            }
           else
            {
                var projectDetails = (from c in ctx.Projects.Include("UserProjects").Include("Users")
                                      where c.StatusId==objProject.StatusId
                                      select new ProjectDTO
                                      {
                                          Project = c,
                                          UsersOfProject = c.UserProjects.ToList(),
                                          Users = ctx.Users.Where(t => c.UserProjects.Any(c2 => c2.UserId == t.EmpId)).ToList(),
                                          Status = ctx.Status.Select(s => s).ToList(),
                                          TaskTypes = ctx.TaskTypes.Select(s => s).ToList()

                                      }
                                 ).AsEnumerable().Select(s => s).ToList();
                projects = projectDetails;
            }

                
                // user.FirstOrDefault().Department = ctx.Departments.Where(s =>  user.Select(c=>c.DeptId).Contains(s.DeptId)).AsEnumerable().Select(s=>s).FirstOrDefault();
                // user.FirstOrDefault().UserTasks = ctx.UserTasks.Where(s => s.UserId == user.FirstOrDefault().EmpId).ToList();  
            
         

            return projects;
        }
        [HttpPost]
        public List<Task> GetAllTasks()
        {
            List<Task> projects = new List<Task>();

            var projectDetails = (from c in ctx.Tasks.Include("Project").Include("User").Include("User1")
                                  select c
                               ).AsEnumerable().Select(s => s).ToList();


            // user.FirstOrDefault().Department = ctx.Departments.Where(s =>  user.Select(c=>c.DeptId).Contains(s.DeptId)).AsEnumerable().Select(s=>s).FirstOrDefault();
            // user.FirstOrDefault().UserTasks = ctx.UserTasks.Where(s => s.UserId == user.FirstOrDefault().EmpId).ToList();  
            projects = projectDetails;


            return projects;
        }

       

        [HttpPost]
        public List<User> GetAllUsers()
        {
            List<User> users = new List<User>();
           
         
                //ctx.Configuration.ProxyCreationEnabled = false;
                //  ctx.Configuration.LazyLoadingEnabled = false;
                var userDetails = (from c in ctx.Users.Include("Department").Include("EmpDesignation").Include("UserRole")
                                   select c).AsEnumerable().Select(s =>s).ToList();


                // user.FirstOrDefault().Department = ctx.Departments.Where(s =>  user.Select(c=>c.DeptId).Contains(s.DeptId)).AsEnumerable().Select(s=>s).FirstOrDefault();
                // user.FirstOrDefault().UserTasks = ctx.UserTasks.Where(s => s.UserId == user.FirstOrDefault().EmpId).ToList();  

                //  users =userDetails;// Commonmethods.UnProxy<List<User>>(ctx,userDetails);
                
                users =userDetails;
          


            return users;
        }

        [HttpPost]
        public List<User> SearchUsers(User objUser)
        {
            List<User> userDetails = ctx.Users.Where(s => s.EmpName.Contains(objUser.EmpName)).Select(s => s).ToList();
            return userDetails;
        }
        [HttpPost]
        public IEnumerable<GeneralDto> GetUserDetails(User objUser)
        {
       
            List<GeneralDto> users = new List<GeneralDto>();
            users.Add(new GeneralDto());
            if(objUser!=null)
            {
                var param = new SqlParameter { ParameterName = "UserId", Value = objUser.EmpId };
             var abc=   ctx.GetUserDetails(objUser.EmpId);
                    using(var connection = ctx.Database.Connection)
                {
                    //connection.Open();
                    var command = connection.CreateCommand();
                    command.CommandText = "EXEC GetUserDetails @UserId";
                    command.Parameters.Add(param);
                    using (var reader = command.ExecuteReader())
                    {
                        var blogs = ((IObjectContextAdapter)ctx)
               .ObjectContext
               .Translate<User>(reader, "Users", System.Data.Entity.Core.Objects.MergeOption.NoTracking).FirstOrDefault();
                        reader.NextResult();
                        var Departments = ((IObjectContextAdapter)ctx)
                 .ObjectContext
                 .Translate<Department>(reader, "Departments", System.Data.Entity.Core.Objects.MergeOption.NoTracking).ToList();
                        reader.NextResult();
                        var UserRoles = ((IObjectContextAdapter)ctx)
              .ObjectContext
              .Translate<UserRole>(reader, "UserRoles", System.Data.Entity.Core.Objects.MergeOption.NoTracking).ToList();
                        reader.NextResult();
                        var EmpDesignation = ((IObjectContextAdapter)ctx)
              .ObjectContext
              .Translate<EmpDesignation>(reader, "EmpDesignations", System.Data.Entity.Core.Objects.MergeOption.NoTracking).ToList();
                        users.FirstOrDefault().User = blogs;
                        users.FirstOrDefault().Departments = Departments;
                        users.FirstOrDefault().UserRole = UserRoles;
                        users.FirstOrDefault().EmpDesignation = EmpDesignation;

                    }
                   
                }
             
            }
          


            return users;
        }

        [HttpPost]
        public IEnumerable<ProjectDTO> GetProjectDetails(Project objProject)
        {

            List<ProjectDTO> projectDetails = new List<ProjectDTO>();
            projectDetails.Add(new ProjectDTO());
            if (objProject != null)
            {
                var param = new SqlParameter { ParameterName = "ProjectId", Value = objProject.ProjectId};
               // var abc = ctx.GetProjectDetails(objProject.ProjectId);
                using (var connection = ctx.Database.Connection)
                {
                    connection.Open();
                   
                    var command = connection.CreateCommand();
                    command.CommandText = "EXEC GetProjectDetails @ProjectId";
                    command.Parameters.Add(param);
                    using (var reader = command.ExecuteReader())
                    {
                        var project = ((IObjectContextAdapter)ctx)
               .ObjectContext
               .Translate<Project>(reader, "Projects", System.Data.Entity.Core.Objects.MergeOption.NoTracking).FirstOrDefault();
                        if(project !=null)
                        {
                            project.Status = ctx.Status.Where(s => s.StatusId == project.StatusId).Select(a => a).ToList().FirstOrDefault();
                        }
                        reader.NextResult();
                        var users = ((IObjectContextAdapter)ctx)
                 .ObjectContext
                 .Translate<UserProject>(reader, "UserProjects", System.Data.Entity.Core.Objects.MergeOption.NoTracking).ToList();
                        reader.NextResult();
                        var allusers = ((IObjectContextAdapter)ctx)
                .ObjectContext
                .Translate<User>(reader, "Users", System.Data.Entity.Core.Objects.MergeOption.NoTracking).ToList();
                        reader.NextResult();
                        var TaskTypes = ((IObjectContextAdapter)ctx).ObjectContext.Translate<TaskType>(reader, "TaskTypes", System.Data.Entity.Core.Objects.MergeOption.NoTracking).ToList();
                        reader.NextResult();
                        var Status = ((IObjectContextAdapter)ctx).ObjectContext.Translate<Status>(reader, "Status", System.Data.Entity.Core.Objects.MergeOption.NoTracking).ToList();

                        projectDetails.FirstOrDefault().Project = project;
                        projectDetails.FirstOrDefault().UsersOfProject = users;
                        projectDetails.FirstOrDefault().Users = allusers;
                        projectDetails.FirstOrDefault().TaskTypes = TaskTypes;
                        projectDetails.FirstOrDefault().Status =Status;

                        connection.Close();
                    }
                  

                }

            }



            return projectDetails;
        }

        [HttpPost]
        public IEnumerable<TaskDTO> GetTaskDetails(Task objTask)
        {

            List<TaskDTO> tasksDetails = new List<TaskDTO>();
            tasksDetails.Add(new TaskDTO());
           
                var param = new SqlParameter { ParameterName = "TaskId", Value = objTask.TaskId };
              
                using (var connection = ctx.Database.Connection)
                {
                    connection.Open();
                    var command = connection.CreateCommand();
                    command.CommandText = "EXEC GetTaskDetails @TaskId";
                    command.Parameters.Add(param);
                    using (var reader = command.ExecuteReader())
                    {
                        var tasks = ((IObjectContextAdapter)ctx)
               .ObjectContext
               .Translate<Task>(reader, "Tasks", System.Data.Entity.Core.Objects.MergeOption.NoTracking).FirstOrDefault();
                        reader.NextResult();
                    if(tasks!=null)
                    {
                        tasks.Project = ((IObjectContextAdapter)ctx)
                 .ObjectContext
                 .Translate<Project>(reader, "Projects", System.Data.Entity.Core.Objects.MergeOption.NoTracking).ToList().FirstOrDefault();
                    }
                         
                        reader.NextResult();
                        var UserTasks= ((IObjectContextAdapter)ctx)
              .ObjectContext
              .Translate<UserTask>(reader, "UserTasks", System.Data.Entity.Core.Objects.MergeOption.NoTracking).ToList();
                        reader.NextResult();
                        var projects = ((IObjectContextAdapter)ctx)
             .ObjectContext
             .Translate<Project>(reader, "Projects", System.Data.Entity.Core.Objects.MergeOption.NoTracking).ToList();
                        reader.NextResult();
                        var users = ((IObjectContextAdapter)ctx)
             .ObjectContext
             .Translate<User>(reader, "Users", System.Data.Entity.Core.Objects.MergeOption.NoTracking).ToList();

                        tasksDetails.FirstOrDefault().Task = tasks;
                        tasksDetails.FirstOrDefault().UsersOfTask = UserTasks;
                        tasksDetails.FirstOrDefault().Projects = projects;
                        tasksDetails.FirstOrDefault().Users = users;
                    connection.Close();
                }

                }

            


            return tasksDetails;
        }
        [HttpPost]
        public void InsertUpdateUser(User objUser)
        {
            if (objUser.EmpId != 0)
            {
                ctx.Entry(objUser).State = System.Data.Entity.EntityState.Modified;
            }
            else
            {
                ctx.Entry(objUser).State = System.Data.Entity.EntityState.Added;

            }
            
            ctx.SaveChanges();
            
        }
        [HttpPost]
        public void InsertUpdateProject(ProjectDTO objProject)
        {
            StringBuilder userXML = new StringBuilder();
            userXML.Append("<Users>");
            foreach (var items in objProject.UsersOfProject)
            {
                userXML.Append("<User>");
                userXML.Append("<UserId>" + items.UserId+ "</UserId>");
                if (objProject.Project.ProjectId != 0)
                    userXML.Append("<ProjectUserId>" + items.ProjectUserId + "</ProjectUserId>");
                userXML.Append("</User>");
            }
            userXML.Append("</Users>");
           
            using (var connection = ctx.Database.Connection)
            {
                //connection.Open();
                ctx.Database.ExecuteSqlCommand(
                             "EXEC InsertUpdateProject @ProjectId, @ProjectName, @ShortName, @Description,@EstimateHours,@StartDate,@EndDate,@TaskTypeId,@CreatedBy,@StatusId,@UserXml",
                             new SqlParameter("@ProjectId", objProject.Project.ProjectId),
                             new SqlParameter("@ProjectName", objProject.Project.ProjectName),
                             new SqlParameter("@ShortName", objProject.Project.ShortName),
                             new SqlParameter("@Description", objProject.Project.Description),
                             new SqlParameter("@EstimateHours", objProject.Project.EstimateHours),
                             new SqlParameter("@StartDate", objProject.Project.StartDate),
                             new SqlParameter("@EndDate", objProject.Project.EndDate),
                             new SqlParameter("@TaskTypeId", objProject.Project.TaskTypeId),
                             new SqlParameter("@CreatedBy", objProject.Project.CreatedBy),
                             new SqlParameter("@StatusId", objProject.Project.StatusId),
                             new SqlParameter("@UserXml",userXML.ToString())

                         );

            }

            }

        [HttpPost]
        public void InsertUpdateTask(TaskDTO objTask)
        {
            StringBuilder userXML = new StringBuilder();
            userXML.Append("<Users>");
            foreach (var items in objTask.UsersOfTask)
            {
                userXML.Append("<User>");
                userXML.Append("<UserId>" + items.UserId + "</UserId>");
                if(items.UserTaskId!=0)
                    userXML.Append("<UserTaskId>" + items.UserTaskId + "</UserTaskId>");
                userXML.Append("</User>");

            }
            userXML.Append("</Users>");

            using (var connection = ctx.Database.Connection)
            {
                //connection.Open();
                ctx.Database.ExecuteSqlCommand(
                             "EXEC InsertUpdateTask @TaskId, @TaskName, @TaskDate, @EstimateDate,@TaskDetails,@ProjectId,@CreatedBy,@Status,@ModifiedBy,@UserXml",
                             new SqlParameter("@TaskId", objTask.Task.TaskId),
                             new SqlParameter("@TaskName", objTask.Task.TaskName),
                             new SqlParameter("@TaskDate", objTask.Task.TaskDate),
                             new SqlParameter("@EstimateDate", objTask.Task.EstimateDate),
                             new SqlParameter("@TaskDetails", objTask.Task.TaskDetails),
                             new SqlParameter("@ProjectId", objTask.Task.ProjectId),
                             new SqlParameter("@CreatedBy", objTask.Task.CreatedBy),
                             new SqlParameter("@Status", objTask.Task.Status),
                             new SqlParameter("@ModifiedBy", objTask.Task.ModifiedBy==null?(object)DBNull.Value:objTask.Task.ModifiedBy),
                             new SqlParameter("@UserXml", userXML.ToString())

                         );

            }

        }
        [HttpPost]
        public void AddDepartment(Department objDept)
        {

                ctx.Entry(objDept).State = System.Data.Entity.EntityState.Added;
            ctx.SaveChanges();

        }
        [HttpPost]
        public void AddDesignation(EmpDesignation objDesg)
        {

            ctx.Entry(objDesg).State = System.Data.Entity.EntityState.Added;
            ctx.SaveChanges();

        }
        [HttpPost]
        public void AddRole(UserRole objRole)
        {
            ctx.Entry(objRole).State = System.Data.Entity.EntityState.Added;
            ctx.SaveChanges();
        }
        [HttpPost]
        public IEnumerable<User> GetUserOfProject(Project objProject)
        {
         var users=  ctx.Database.SqlQuery<User>("exec GetProjectUsers @ProjectId", new SqlParameter { ParameterName = "ProjectId", Value = objProject.ProjectId }).ToList();

            return users;

        }
        [HttpPost]
        public void UpdateUserPassword(User objUser)
        {
           

            using (var connection = ctx.Database.Connection)
            {
                //connection.Open();
                ctx.Database.ExecuteSqlCommand(
                             "EXEC UpdateUserPassword @UserId @Password",
                             new SqlParameter("@UserId", objUser.EmpId),
                             new SqlParameter("@Password", objUser.Password)
                         );

            }

        }

    }
}
